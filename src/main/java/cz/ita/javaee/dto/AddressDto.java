package cz.ita.javaee.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class AddressDto {

    @NotNull
    @Length(min = 1, max = 50)
    private String streetName;

    @NotNull
    @Length(min = 1, max = 5)
    private String houseNumber;

    @NotNull
    @Length(min = 1, max = 50)
    private String city;

    @NotNull
    @Length(min = 1, max = 10)
    private String zipCode;

    @NotNull
    @Length(min = 3, max = 3)
    private String country;


    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
