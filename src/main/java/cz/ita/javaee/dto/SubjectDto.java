package cz.ita.javaee.dto;

public class SubjectDto extends AbstractDto {
    private boolean active;
    private String note;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
