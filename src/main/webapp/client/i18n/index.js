'use strict';
import _ from "lodash";
import {vsprintf} from "sprintf-js";

const messages = {
  'error.originalPassword.invalid': 'Nesprávne aktuálne heslo.',
  'error.confirmPassword.invalid': 'Heslá sa nezhodujú.',
  'error.auth.invalid': 'Neoprávnený prístup.',
  'error.auth.login': 'Neplatné prihlasovacie údaje.',
  'error.user.delete.myself': 'Nie je možné zmazať prihláseného užívateľa.',
  'error.user.edit.myself': 'Nie je možné editovať prihláseného užívateľa.',
  'error.company.deactivate': 'Nie je možné deaktovať firmu.',
  'error.activate.edit.myself': 'Nie je možné aktivovať prihláseného užívateľa.',
  'error.deactivate.edit.myself': 'Nie je možné deaktivovať prihláseného užívateľa.',
  'error.codeList.deactivate.invalid': 'Zneplatnenie položky nebolo možné vykonať.',
  'error.data.invalid': 'Dáta sa nepodarilo spracovať.',
  'error.data.integrity.violation': 'Akciu nie je možné vykonať, pretože by bola porušená integrita dát.',
  'error.data.integrity.violation.referenced': 'Položku nie je možné odstrániť, pretože je používaná v iných záznamoch.',
  'error.data.integrity.violation.duplicate': 'Akciu nie je možné vykonať. V aplikácii už existuje záznam s rovnakou hodnotou.',
  'error.order.empty': 'Objednávku nie je možné vytvoriť. Musí obsahovať aspoň jendu položku na objednanie.',
  'codeList.delete.confirmation': 'Naozaj chcete odstrániť položku "%s"?',
  'user.delete.confirmation': 'Naozaj chcete odstrániť užívateľa "%s"?',
  'company.delete.confirmation': 'Naozaj chcete odstrániť firmu "%s"?',
  'company.delete.success': 'Subjekt bol úspešne zmazaný.',
  'company.activate.success': 'Subjekt bol úspešne aktivovaný.',
  'company.deactivate.success': 'Subjekt bol úspešne deaktivovaný.',
  'company.edit.success': 'Zákazník bol uložený.'
};

export default {
  message(key, defaultMessageOrObjectArgs, defaultMessageParam) {
    const defaultMessage = _.isString(defaultMessageOrObjectArgs) ? defaultMessageOrObjectArgs : defaultMessageParam;
    let message = messages[key] || (defaultMessage ? (messages[defaultMessage] || defaultMessage) : key);
    if (_.isArray(defaultMessageOrObjectArgs)) {
      message = vsprintf(message, defaultMessageOrObjectArgs);
    }
    return message;
  }
}
