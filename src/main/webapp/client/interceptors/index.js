'use strict';
import Vue from 'vue';
import errorInterceptor from './errorInterceptor'

export default (store, router) => {
  Vue.http.interceptors.push(errorInterceptor());
};
