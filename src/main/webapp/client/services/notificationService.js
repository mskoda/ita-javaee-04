'use strict';

import Vue from 'vue';
import VueNotifications from 'vue-notifications';
import miniToastr from 'mini-toastr';
import i18n from 'i18n/';

export default {
  init() {
    // If using mini-toastr, provide additional configuration
    const toastTypes = {
      success: 'success',
      error: 'error',
      info: 'info',
      warn: 'warn'
    };

    miniToastr.init({types: toastTypes});

    // Here we setup messages output to `mini-toastr`
    function toast ({title, message, type, timeout, cb}) {
      return miniToastr[type](message, title, timeout, cb)
    }

    const options = {
      success: toast,
      error: toast,
      info: toast,
      warn: toast
    };
    // Activate plugin
    Vue.use(VueNotifications, options);// VueNotifications have auto install but if we want to specify options we've got to do it manually.
  },
  info(key, defaultMessage) {
    VueNotifications.info({message: i18n.message(key, defaultMessage)});
  },
  success(key, defaultMessage) {
    VueNotifications.success({message: i18n.message(key, defaultMessage)});
  },
  error(key, args, defaultMessage) {
    VueNotifications.error({message: i18n.message(key, args, defaultMessage)});
  }
}
