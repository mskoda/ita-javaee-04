import Vue from 'vue';
import currencyFilter from './currencyFilter';
import dateFilter from './dateFilter';
import codeListValueFilter from './codeListValueFilter';

export default () => {
  Vue.filter('currency', currencyFilter);
  Vue.filter('date', dateFilter);
  Vue.filter('codeListValue', codeListValueFilter);
};
