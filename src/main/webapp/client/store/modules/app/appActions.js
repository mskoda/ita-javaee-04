import {APP_CONFIGURATION_LOAD, APP_SUBMIT_PROTECTION_ENABLE, APP_SUBMIT_PROTECTION_DISABLE} from 'store/mutationTypes';

export default {
  async protect ({commit}) {
    await commit(APP_SUBMIT_PROTECTION_ENABLE);
  },
  async unprotect ({commit}) {
    await commit(APP_SUBMIT_PROTECTION_DISABLE);
  }
}
