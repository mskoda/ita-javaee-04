import { APP_CONFIGURATION_LOAD, APP_SUBMIT_PROTECTION_ENABLE, APP_SUBMIT_PROTECTION_DISABLE } from '../../mutationTypes';

const mutations = {
  async [APP_CONFIGURATION_LOAD](state, action){
    state.configuration = action.configuration;
  },
  async [APP_SUBMIT_PROTECTION_ENABLE](state){
    state.submitProtection = true;
  },
  async [APP_SUBMIT_PROTECTION_DISABLE](state){
    state.submitProtection = false;
  }
};

export default mutations;
