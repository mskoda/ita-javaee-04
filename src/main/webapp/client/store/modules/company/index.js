import actions from './companyActions';
import mutations from './companyMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
